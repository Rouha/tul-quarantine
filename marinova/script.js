let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

var engine = null;
let scene = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

var createScene = function () {
	var scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

	//let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	//light.intensity = 0;

	// CAMERA
	let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(-15, 3, 0), scene);
	camera.radius = 5;
	camera.minZ = 0.1;
	camera.maxZ = 50;
	camera.speed = 0.05;
	camera.setTarget(new BABYLON.Vector3(-14, 3, 0));
	camera.speed = 0.05;
	camera.fov = 0.8;
	//camera.viewport = 10;
	camera.inverseRotationSpeed = 0.5;
	camera.ellipsoid = new BABYLON.Vector3(0.1, 0.1, 0.1);
	camera.ellipsoidOffset = new BABYLON.Vector3(0, 0, 0);
	camera.checkCollisions = true;
	camera.attachControl(canvas, true);

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			stackPanel.isVisible = true;
		} else {
			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
		}
		cameraLoop();
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			camera.position.x = -15;
			camera.position.y = 3;
			camera.position.z = 0;
			camera.setTarget(new BABYLON.Vector3(-14, 3, 0));
		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

	var box1 = BABYLON.Mesh.CreateBox("Box1", 0.1, scene);
	box1.position = new BABYLON.Vector3(3.76, -48, -0.61);
	var materialBox = new BABYLON.StandardMaterial("texture1", scene);
	materialBox.diffuseColor = new BABYLON.Color3(1, 0, 0);
	materialBox.alpha = 0;

	box1.material = materialBox;

	function cameraLoop() {
		var distanceK = BABYLON.Vector3.Distance(camera.position, box1.position);
		svetluska.intensity = distanceK;
		console.log(svetluska.intensity);

		if (distanceK < 6) {
			camera.position = new BABYLON.Vector3(-0.07, 1.78, 3.66);
			camera.setTarget(new BABYLON.Vector3(-0.07, 1.78, 0));
		}

		//console.log(camera.position.x, camera.position.y, camera.position.z, distanceK);
	}

	// ADD WASD KEYS
	camera.keysUp.push(87);
	camera.keysDown.push(83);
	camera.keysLeft.push(65);
	camera.keysRight.push(68);

	scene.onPointerDown = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	// GRAVITATION
	scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
	camera.applyGravity = true;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = "100%";
	boxOne.height = "40px";
	boxOne.paddingBottomInPixels = 1;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = "black";
	boxOneText.alpha = 1;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	textOne.color = "white";
	textOne.fontSize = 16;
	textOne.text = "ovládání w, a, s, d + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | Eliza Marinova";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	// NAHRAVANI MODELU
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

	BABYLON.SceneLoader.Append("./", "model.glb", scene);

	// Wait for textures and shaders to be ready
	scene.executeWhenReady(function () {
		for (var y = 0; y < scene.meshes.length; y++) {
			scene.meshes[y].checkCollisions = true;
			scene.meshes[y].edgesWidth = 3;
			scene.meshes[y].edgesColor = new BABYLON.Color4(1, 0, 0, 1);
			scene.meshes.alpha = 0;
		}
		console.log(scene);
		var gltfScene = scene.meshes[0];
		var gltfAnimations = scene.animationGroups[0];



		console.log(scene);
		console.log(gltfScene);


		for (var i = 0; i < scene.animationGroups.length; i++) {

			scene.animationGroups[i].start(true, 1, 1, 3150);
		}

		console.log(scene.lights);

		// SHADOW GENERATOR for LIGHTS
		// generators
		for (var j = 0; j < scene.lights.length; j++) {
			var generators = [];
			generators.push(j);
			generators[j] = new BABYLON.ShadowGenerator(512, scene.lights[j]);
			generators[j].useBlurExponentialShadowMap = true;
		}

		// RECEIVING SHADOWS for MESHES
		for (var k = 0; k < scene.meshes.length; k++) {
			scene.meshes[k].receiveShadows = true;
		}

		// Once the scene is loaded, just register a render loop to render it
		engine.runRenderLoop(function () {
			scene.render();
		})
	});

	// VIDEOTEXTURA
	var planeOpts = {
		height: 5.616,
		width: 9.984,
		sideOrientation: BABYLON.Mesh.BACKSIDE
	};
	var monitorVideo = BABYLON.MeshBuilder.CreatePlane("plane", planeOpts, scene);
	var vidPos = (new BABYLON.Vector3(-7, 4, 6));
	monitorVideo.position = vidPos;
	monitorVideo.rotation.x = Math.PI;

	var monitorVideo2 = BABYLON.MeshBuilder.CreatePlane("plane", planeOpts, scene);
	var vidPos2 = (new BABYLON.Vector3(5, 4, -1));
	monitorVideo2.position = vidPos2;
	monitorVideo2.rotation.x = Math.PI;
	monitorVideo2.rotation.y = Math.PI / 2;
	//monitorVideo2.rotation.z = Math.PI / 2;

	var monitorVideoMat = new BABYLON.StandardMaterial("m", scene);
	monitorVideoMat.specularColor = new BABYLON.Color3(0, 0, 0);
	monitorVideoMat.emissiveColor = new BABYLON.Color3(1, 1, 1);
	var monitorVideoVidTex = new BABYLON.VideoTexture("videotexture", "video/video.mp4", scene, true, true, BABYLON.VideoTexture.TRILINEAR_SAMPLINGMODE, {
		autoUpdateTexture: true
	});

	monitorVideoMat.diffuseTexture = monitorVideoVidTex;
	monitorVideoMat.roughness = 1;
	monitorVideoMat.emissiveColor = new BABYLON.Color3.White();
	monitorVideo.material = monitorVideoMat;
	monitorVideo2.material = monitorVideoMat;

	scene.onPointerUp = function () {
		monitorVideoVidTex.video.play();
	};

	// ZVUK
	// Load the sound and play it automatically once ready
	var music = new BABYLON.Sound("Music", "sounds/sound.ogg", scene, null, {
		loop: true,
		autoplay: true
	});

	// SHOW AXIS SNIPPET
	var showAxis = function (size) {
		var makeTextPlane = function (text, color, size) {
			var dynamicTexture = new BABYLON.DynamicTexture("DynamicTexture", 50, scene, true);
			dynamicTexture.hasAlpha = true;
			dynamicTexture.drawText(text, 5, 40, "bold 36px Arial", color, "transparent", true);
			var plane = new BABYLON.Mesh.CreatePlane("TextPlane", size, scene, true);
			plane.material = new BABYLON.StandardMaterial("TextPlaneMaterial", scene);
			plane.material.backFaceCulling = false;
			plane.material.specularColor = new BABYLON.Color3(0, 0, 0);
			plane.material.diffuseTexture = dynamicTexture;
			return plane;
		};

		var axisX = BABYLON.Mesh.CreateLines("axisX", [
			new BABYLON.Vector3.Zero(), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, 0.05 * size, 0),
			new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)
		], scene);
		axisX.color = new BABYLON.Color3(1, 0, 0);
		var xChar = makeTextPlane("X", "red", size / 10);
		xChar.position = new BABYLON.Vector3(0.9 * size, -0.05 * size, 0);
		var axisY = BABYLON.Mesh.CreateLines("axisY", [
			new BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(-0.05 * size, size * 0.95, 0),
			new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3(0.05 * size, size * 0.95, 0)
		], scene);
		axisY.color = new BABYLON.Color3(0, 1, 0);
		var yChar = makeTextPlane("Y", "green", size / 10);
		yChar.position = new BABYLON.Vector3(0, 0.9 * size, -0.05 * size);
		var axisZ = BABYLON.Mesh.CreateLines("axisZ", [
			new BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, -0.05 * size, size * 0.95),
			new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3(0, 0.05 * size, size * 0.95)
		], scene);
		axisZ.color = new BABYLON.Color3(0, 0, 1);
		var zChar = makeTextPlane("Z", "blue", size / 10);
		zChar.position = new BABYLON.Vector3(0, 0.05 * size, 0.9 * size);
	};
	//showAxis(10);
	// SHOW AXIS SNIPPET

	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
