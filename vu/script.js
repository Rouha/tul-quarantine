let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

var engine = null;
let scene = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

var createScene = function () {
	var scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

	//let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	//light.intensity = 0;

	// CAMERA
	let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(-23, 4, 0), scene);
	camera.radius = 5;
	camera.minZ = 0.01;
	camera.maxZ = 500;
    camera.setTarget(new BABYLON.Vector3(24.6, 5.2, 1.13));
	camera.speed = 0.15;
	camera.fov = 0.8;
	//camera.viewport = 10;
	camera.inverseRotationSpeed = 0.5;
	camera.ellipsoid = new BABYLON.Vector3(0.3, 2, 0.3);
	camera.ellipsoidOffset = new BABYLON.Vector3(0, 0, 0);
	camera.checkCollisions = true;
	camera.attachControl(canvas, true);

	var svetluska = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 1, 0), scene);
	svetluska.intensity = 4;
	svetluska.range = 10;
    svetluska.parent = camera;

    var box1 = BABYLON.Mesh.CreateBox("Box1", 0.1, scene);
    box1.position = new BABYLON.Vector3(24.6, 5.2, 1.13);
    var materialBox = new BABYLON.StandardMaterial("texture1", scene);
    materialBox.diffuseColor = new BABYLON.Color3(1, 0, 0);
    materialBox.alpha = 0;

    box1.material = materialBox;

    //ZVUK
    var music = new BABYLON.Sound("Music", "./sounds/sound.mp3", scene, null, {
        loop: false,
        autoplay: false
    });

    let autoPlayStatus = 0;

    //ANIMACE A ZVUK NA PŘIBLÍŽENÍ
    function cameraLoop() {
        var distanceC = BABYLON.Vector3.Distance(camera.position, box1.position);

        if (distanceC < 15 && autoPlayStatus === 0) {
			for (var i = 0; i < scene.animationGroups.length; i++) {
				scene.animationGroups[i].play();
			}

			music.play();
			autoPlayStatus = 1;
		}
    }
	cameraLoop();

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			camera.attachControl(canvas, true);
			stackPanel.isVisible = true;
		} else {
			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
		}
		cameraLoop();
		console.log(autoPlayStatus);
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			camera.position.x = -23;
			camera.position.y = 4;
			camera.position.z = 0;
			camera.setTarget(new BABYLON.Vector3(24.6, 5.2, 1.13));

		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

	// This attaches the camera to the canvas
	camera.keysUp.push(87);
	camera.keysDown.push(83);
	camera.keysLeft.push(65);
	camera.keysRight.push(68);

	scene.onPointerDown = function (evt) {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	// GRAVITACE
	scene.gravity = new BABYLON.Vector3(0, -0.5, 0);
	camera.applyGravity = true;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = "100%";
	boxOne.height = "40px";
	boxOne.paddingBottomInPixels = 1;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = "black";
	boxOneText.alpha = 1;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	textOne.color = "white";
	textOne.fontSize = 16;
	textOne.text = "ovládání w, a, s, d + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | Hong Van Vu";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	//NAHRAVANI MODELU
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

    BABYLON.SceneLoader.ImportMesh("", "./", "model.glb", scene, function (meshes) {

        var mujimport0 = meshes;
        //console.log(mujimport0);
        mujimport0.position = new BABYLON.Vector3(0, 0, 0);
        mujimport0.scaling = new BABYLON.Vector3(1, 1, 1);
        mujimport0.rotation = new BABYLON.Vector3(0, Math.PI * 2, 0);

        for (var r = 0; r < scene.animationGroups.length; r++) {
            scene.animationGroups[r].loopAnimation = false;
        }

        for (var y = 0; y < mujimport0.length; y++) {
            mujimport0[y].checkCollisions = true;
		}
        //console.log(scene);

		// SHADOW GENERATOR for LIGHTS

		// generators
		for (var j = 1; j < scene.lights.length; j++) {
			var generators = [];
			generators.push(i);
            generators[i] = new BABYLON.ShadowGenerator(512, scene.lights[j]);
			generators[i].useBlurExponentialShadowMap = true;
			generators[i].addShadowCaster();
		}

		// RECEIVING SHADOWS for MESHES
		for (var k = 1; k < meshes.length; k++) {
			meshes[k].receiveShadows = true;
		}

    });

	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
    }
});

window.addEventListener("resize", function () {
	engine.resize();
});

