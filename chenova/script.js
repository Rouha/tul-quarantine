let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

var engine = null;
let scene = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

var createScene = function () {
	var scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

	//let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	//light.intensity = 0;

	// CAMERA
	let camera = new BABYLON.ArcRotateCamera("Camera", 3 * Math.PI / 2, 1.2, 20, BABYLON.Vector3.Zero(), scene);
	camera.radius = 5;
	camera.minZ = 0.1;
	camera.maxZ = 50;
	camera.setTarget(new BABYLON.Vector3(0, 0, 0));
	camera.speed = 0.1;
	camera.fov = 0.8;
	camera.inverseRotationSpeed = 0.5;

	// Creation of a torus
	// (name, diameter, thickness, tessellation, scene, updatable)
	var torus = BABYLON.Mesh.CreateTorus("torus", 0.1, 0.1, 0.1, scene, false);
	torus.position = new BABYLON.Vector3(0,0,0);
	camera.setTarget(torus);

	let freeCamera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 1, 0), scene);
	freeCamera.ellipsoid = new BABYLON.Vector3(0.1, 0.1, 0.1);
	freeCamera.ellipsoidOffset = new BABYLON.Vector3(0, 1, 1);
	freeCamera.checkCollisions = true;
	freeCamera.radius = 5;
	freeCamera.minZ = 0.1;
	freeCamera.maxZ = 50;
	freeCamera.setTarget(new BABYLON.Vector3(0, 1, 1));
	freeCamera.speed = 0.05;
	freeCamera.fov = 0.8;
	freeCamera.inverseRotationSpeed = 0.4;

	var svetluska = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 1, 0), scene);
	svetluska.intensity = 4;
	svetluska.range = 10;
	svetluska.parent = freeCamera;

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
			setTimeout(freeCameraEnable, 5000);
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			freeCamera.attachControl(canvas, true);
			stackPanel.isVisible = true;
		} else {
			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
		}
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "f")
			freeCamera.position.y -= 0.04;
		if (keyState === "r")
			freeCamera.position.y += 0.04;
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			freeCamera.position.x = 0;
			freeCamera.position.y = 1;
			freeCamera.position.z = 0;
			freeCamera.setTarget(new BABYLON.Vector3(0, 1, 1));
		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

	//console.log(camera.position);

	// This attaches the camera to the canvas
	freeCamera.keysUp.push(87);
	freeCamera.keysDown.push(83);
	freeCamera.keysLeft.push(65);
	freeCamera.keysRight.push(68);

	// CAMERA SWITCH
	function freeCameraEnable() {
		scene.activeCamera = freeCamera;
		freeCamera.attachControl(canvas, true);
	}

	// GRAVITACE
	scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
	freeCamera.applyGravity = false;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	let boxWidth = "100%";
	let boxHeight = "40px";
	let boxBackground = "black";
	let boxAlpha = 0.75;
	let boxColor = "white";

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = boxWidth;
	boxOne.height = boxHeight;
	boxOne.paddingBottomInPixels = 1;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = boxBackground;
	boxOneText.alpha = boxAlpha;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	textOne.color = boxColor;
	textOne.fontSize = 16;
	textOne.text = "ovládání w, a, s, d, r, f + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | Dajana Chenová";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	//NAHRAVANI MODELU
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

	BABYLON.SceneLoader.Append("./", "model.glb", scene);
	// Wait for textures and shaders to be ready
	scene.executeWhenReady(function () {
		for (var y = 0; y < scene.meshes.length; y++) {
			scene.meshes[y].checkCollisions = true;
		}
		//console.log(scene);
		var gltfScene = scene.meshes[0];
		var gltfAnimations = scene.animationGroups[0];

		for (var i = 0; i < scene.animationGroups.length; i++) {
			scene.animationGroups[i].start(true);
		}

		// SHADOW GENERATOR for LIGHTS

		// generators
		for (var j = 1; j < scene.lights.length; j++) {
			var generators = [];
			generators.push(i);
			generators[i] = new BABYLON.ShadowGenerator(512, scene.lights[j]);
			generators[i].useBlurExponentialShadowMap = true;
			generators[i].addShadowCaster(torus);
		}

		// RECEIVING SHADOWS for MESHES
		for (var k = 1; k < scene.meshes.length; k++) {
			scene.meshes[k].receiveShadows = true;
		}

		// Once the scene is loaded, just register a render loop to render it
		engine.runRenderLoop(function () {
			//console.log(scene);
			//console.log(gltfScene);
			scene.render();
		});
	});

	//scene.rotation.y = 2 * Math.PI / 4;

	scene.beforeRender = function () {
		scene.activeCamera.alpha += .01;
	};

	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
