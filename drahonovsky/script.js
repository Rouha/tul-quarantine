let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

var engine = null;
let scene = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

var createScene = function () {
	var scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

	//let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	//light.intensity = 0;

	// CAMERA
	let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 1, 0), scene);
	camera.radius = 5;
	camera.minZ = 0.1;
	camera.maxZ = 50;
	camera.setTarget(new BABYLON.Vector3(0, 1, 1));
	camera.speed = 0.1;
	camera.fov = 0.8;
	//camera.viewport = 10;
	camera.inverseRotationSpeed = 0.5;
	camera.ellipsoid = new BABYLON.Vector3(0.1, 0.1, 0.1);
	camera.ellipsoidOffset = new BABYLON.Vector3(0, 0, 0);
	camera.checkCollisions = true;
	camera.attachControl(canvas, true);

	var svetluska = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 1, 0), scene);
	svetluska.intensity = 4;
	svetluska.range = 10;
	svetluska.parent = camera;

	function cameraLoop() {
		console.log(camera.position.x, camera.position.y, camera.position.z);
		setTimeout(cameraLoop, 100);
	}
	cameraLoop();

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function gameLoop() {
		if (keyState === "f") {
			camera.position.y -= 0.04;
		}
		if (keyState === "r") {
			camera.position.y += 0.04;
		}
		if (keyState === "z") {
			window.location.replace("../");
		}
		setTimeout(gameLoop, 5);
	}
	gameLoop();

	// This attaches the camera to the canvas
	camera.keysUp.push(87);
	camera.keysDown.push(83);
	camera.keysLeft.push(65);
	camera.keysRight.push(68);

	scene.onPointerDown = function (evt) {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	// GRAVITACE
	scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
	camera.applyGravity = false;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	let boxWidth = "200px";
	let boxHeight = "40px";
	let boxBackground = "black";
	let boxAlpha = 0.4;
	let boxColor = "white";

	let cursorTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	let cursor = new BABYLON.GUI.Rectangle();
	cursor.height = "15px";
	cursor.width = "15px";
	cursor.color = "blue";
	cursor.thickness = 3;
	cursorTexture.addControl(cursor);

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.width = "200px";
	stackPanel.height = "100%";
	stackPanel.horizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = boxWidth;
	boxOne.height = boxHeight;
	boxOne.paddingBottomInPixels = 10;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = boxBackground;
	boxOneText.alpha = boxAlpha;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
	textOne.color = boxColor;
	textOne.fontSize = 24;
	textOne.text = "Roman";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	//NAHRAVANI MODELU
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

	BABYLON.SceneLoader.Append("./", "model.glb", scene);
	// Wait for textures and shaders to be ready
	scene.executeWhenReady(function () {
		for (var y = 0; y < scene.meshes.length; y++) {
			scene.meshes[y].checkCollisions = true;
		}
		console.log(scene);
		var gltfScene = scene.meshes[0];
		var gltfAnimations = scene.animationGroups[0];

		for (var i = 0; i < scene.animationGroups.length; i++) {
			scene.animationGroups[i].start(true);
		}

		// SHADOW GENERATOR for LIGHTS

		// generators
		for (var j = 1; j < scene.lights.length; j++) {
			var generators = [];
			generators.push(i);
			generators[i] = new BABYLON.ShadowGenerator(512, scene.lights[j]);
			generators[i].useBlurExponentialShadowMap = true;
			generators[i].addShadowCaster(torus);
		}

		// RECEIVING SHADOWS for MESHES
		for (var k = 1; k < scene.meshes.length; k++) {
			scene.meshes[k].receiveShadows = true;
		}

		// Once the scene is loaded, just register a render loop to render it
		engine.runRenderLoop(function () {
			//console.log(scene);
			//console.log(gltfScene);
			scene.render();
		});
	});

	//scene.rotation.y = 2 * Math.PI / 4;
	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
