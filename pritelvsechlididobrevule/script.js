let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

let engine = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

let createScene = function () {
    let scene = new BABYLON.Scene(engine);
    scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

    //let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
    //light.intensity = 3;

    // CAMERA
    let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(-0.07, 1.78, 3.66), scene);
    camera.radius = 5;
    camera.minZ = 0.1;
    camera.maxZ = 300;
    camera.setTarget(new BABYLON.Vector3(-0.07, 1.78, 0));
    camera.speed = 0.05;
    camera.fov = 0.8;
    //camera.viewport = 10;
    camera.inverseRotationSpeed = 0.5;
    camera.ellipsoid = new BABYLON.Vector3(0.12, 0.12, 0.12);
    camera.ellipsoidOffset = new BABYLON.Vector3(0, 0, 0);
	camera.checkCollisions = true;
    camera.attachControl(canvas, true);

    var svetluska = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 1, 0), scene);
    svetluska.intensity = 1;
    svetluska.range = 0.1;
    svetluska.parent = camera;

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			stackPanel.isVisible = true;
		} else {
			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
		}
		cameraLoop();
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "f")
			camera.position.y -= 0.04;
		if (keyState === "r")
			camera.position.y += 0.04;
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			camera.position.x = -0.07;
			camera.position.y = 1.78;
			camera.position.z = 3.66;
			camera.setTarget(new BABYLON.Vector3(-0.07, 1.78, 0));
		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

    var box1 = BABYLON.Mesh.CreateBox("Box1", 0.1, scene);
    box1.position = new BABYLON.Vector3(3.76, -48, -0.61);
    var materialBox = new BABYLON.StandardMaterial("texture1", scene);
    materialBox.diffuseColor = new BABYLON.Color3(1, 0, 0);
    materialBox.alpha = 0;

    box1.material = materialBox;

    function cameraLoop() {
        var distanceK = BABYLON.Vector3.Distance(camera.position, box1.position);
        svetluska.intensity = distanceK;
		console.log(svetluska.intensity);

        if (distanceK < 6) {
            camera.position = new BABYLON.Vector3(-0.07, 1.78, 3.66);
            camera.setTarget(new BABYLON.Vector3(-0.07, 1.78, 0));
        }

		//console.log(camera.position.x, camera.position.y, camera.position.z, distanceK);
	}

    // ADD WASD KEYS
    camera.keysUp.push(87);
    camera.keysDown.push(83);
    camera.keysLeft.push(65);
    camera.keysRight.push(68);

    scene.onPointerDown = function () {
        if (!engine.isFullscreen) {
            engine.switchFullscreen(true); //true = requestPointerLock.
        }
        else if (!engine.isPointerLock && canvas.requestPointerLock) {
            canvas.requestPointerLock();
        }
    };

    // GRAVITATION
    scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
    camera.applyGravity = false;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	let boxWidth = "100%";
	let boxHeight = "40px";
	let boxBackground = "black";
	let boxAlpha = 0.75;
	let boxColor = "white";

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = boxWidth;
	boxOne.height = boxHeight;
	boxOne.paddingBottomInPixels = 1;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = boxBackground;
	boxOneText.alpha = boxAlpha;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	textOne.color = boxColor;
	textOne.fontSize = 16;
	textOne.text = "ovládání w, a, s, d, r, f + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | PřítelVšechLidíDobréVůle";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	// MODEL LOADER
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

	BABYLON.SceneLoader.Append("", "model.glb", scene);

	// WAIT FOR TEXTURES AND SHADERS
	scene.executeWhenReady(function () {
		for (let y = 0; y < scene.meshes.length; y++) {
			scene.meshes[y].checkCollisions = true;
			//scene.meshes[y].enableEdgesRendering();
			//scene.meshes[y].edgesWidth = 1.0;
			//scene.meshes[y].edgesColor = new BABYLON.Color4(1, 0, 0, 1);
        }



		console.log(scene);


		// PLAY WHOLE SCENE ANIMATIONS IN LOOP
		for (let i = 0; i < scene.animationGroups.length; i++) {
			scene.animationGroups[i].start(true);
		}



		engine.runRenderLoop(function () {
			//console.log(scene);
			//console.log(gltfScene);
			scene.render();
		});
	});

	// ZVUKY
    var sound1 = new BABYLON.Sound("", "sounds/sound1.mp3", scene, null, {
		loop: true,
		autoplay: true,
		spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound1.setPosition(new BABYLON.Vector3(7.1,-4.1,0.04));

    var sound2 = new BABYLON.Sound("", "sounds/sound2.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound2.setPosition(new BABYLON.Vector3(0.2,-18.4,-0.5));

    var sound3 = new BABYLON.Sound("", "sounds/sound3.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound3.setPosition(new BABYLON.Vector3(7.6,-8.1,1.4));

    var sound4 = new BABYLON.Sound("", "sounds/sound4.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound4.setPosition(new BABYLON.Vector3(5.0,-4.1,3.7));

    var sound5 = new BABYLON.Sound("", "sounds/sound5.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound5.setPosition(new BABYLON.Vector3(2.5,-2.1,3.37));

    var sound6 = new BABYLON.Sound("", "sounds/sound6.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound6.setPosition(new BABYLON.Vector3(4.2,-19.4,-4.8));

    var sound7 = new BABYLON.Sound("", "sounds/sound7.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound7.setPosition(new BABYLON.Vector3(2.5,-18.4,3.4));

    var sound8 = new BABYLON.Sound("", "sounds/sound8.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound8.setPosition(new BABYLON.Vector3(2.7,-10.2,-4.7));

    var sound9 = new BABYLON.Sound("", "sounds/sound9.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound9.setPosition(new BABYLON.Vector3(7.1,-11.9,0.9));

    var sound10 = new BABYLON.Sound("", "sounds/sound10.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound10.setPosition(new BABYLON.Vector3(-0.01,-6.2,-0.2));

    var sound11 = new BABYLON.Sound("", "sounds/sound11.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound11.setPosition(new BABYLON.Vector3(2.8,-18,3.5));

    var sound12 = new BABYLON.Sound("", "sounds/sound12.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound12.setPosition(new BABYLON.Vector3(4.64,2.7,3.4));

    var sound13 = new BABYLON.Sound("", "sounds/sound13.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound13.setPosition(new BABYLON.Vector3(4.6,-12.2,3.3));

    var sound14 = new BABYLON.Sound("", "sounds/sound14.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound14.setPosition(new BABYLON.Vector3(2.8, -22.6, -4.8));

    var sound15 = new BABYLON.Sound("", "sounds/sound15.mp3", scene, null, {
        loop: true,
        autoplay: true,
        spatialSound: true,
        distanceModel: "linear",
        maxDistance: 4
    });
    sound15.setPosition(new BABYLON.Vector3(7.2,-28.2,0.05));
    //ZVUKY - KONEC




    var lightR1 = new BABYLON.PointLight("Omni", new BABYLON.Vector3(1.327, 7.697, 5.984), scene);
    lightR1.range = 1;
    lightR1.intensity = 0;

    function lightsLoop() {
        var distance1 = BABYLON.Vector3.Distance(camera.position, sound1.position);

        console.log(distance1);

        if (distance1 < 15) {
            lightR1.intensity = 0;
        };
        setTimeout(lightsLoop, 1000);
    };
    lightsLoop();







    //function lightsLoop() {
    //    console.log(camera.position.x, camera.position.y, camera.position.z);
    //    setTimeout(cameraLoop, 100);
    //}
    //cameraLoop();



	//for (let l = 0; l < coordinates[0].length; l++) {
	//	let lightRoom = new BABYLON.PointLight("Omni", new BABYLON.Vector3(coordinates[l]), scene);
	//	lightRoom[l].intensity = 5;
	//	lightRoom[l].range = 5;
	//}

	return scene;
};

// BABYLON AFTER
engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
