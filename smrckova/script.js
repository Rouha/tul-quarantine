let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

var engine = null;
let scene = null;
let sceneToRender = null;
let actualName = "none";

let createDefaultEngine = function () { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

var createScene = function () {
	var scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(0, 0, 0, 0);

	//let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	//light.intensity = 0;

	// CAMERA
	let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, 2, 0), scene);
	camera.radius = 1;
	camera.minZ = 0.1;
	camera.maxZ = 50;
	camera.setTarget(new BABYLON.Vector3(0, 1, 1));
	camera.speed = 0.1;
	camera.fov = 0.8;
	//camera.viewport = 10;
	camera.inverseRotationSpeed = 0.5;
	camera.ellipsoid = new BABYLON.Vector3(0.01, 0.01, 0.01);
	camera.ellipsoidOffset = new BABYLON.Vector3(0, 0, 0);
	camera.checkCollisions = false;
	camera.attachControl(canvas, true);

	var svetluska = new BABYLON.PointLight("Omni", new BABYLON.Vector3(0, 1, 0), scene);
	svetluska.intensity = 4;
	svetluska.range = 10;
	svetluska.parent = camera;

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			camera.attachControl(canvas, true);
			stackPanel.isVisible = true;
		} else {
			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
		}
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			camera.position.x = -23;
			camera.position.y = 4;
			camera.position.z = 0;
			camera.setTarget(new BABYLON.Vector3(24.6, 5.2, 1.13));

		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

	// This attaches the camera to the canvas
	camera.keysUp.push(87);
	camera.keysDown.push(83);
	camera.keysLeft.push(65);
	camera.keysRight.push(68);

	scene.onPointerDown = function (evt) {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	// GRAVITACE
	scene.gravity = new BABYLON.Vector3(0, -0.5, 0);
	camera.applyGravity = false;

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	//ONE
	let boxOne = new BABYLON.GUI.Container('containerOne');
	boxOne.width = "100%";
	boxOne.height = "40px";
	boxOne.paddingBottomInPixels = 1;
	stackPanel.addControl(boxOne);

	let boxOneText = new BABYLON.GUI.Container('containerOneText');
	boxOneText.background = "black";
	boxOneText.alpha = 1;
	boxOne.addControl(boxOneText);

	let textOne = new BABYLON.GUI.TextBlock();
	textOne.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	textOne.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	textOne.color = "white";
	textOne.fontSize = 16;
	textOne.text = "ovládání w, a, s, d + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | Kateřina Smrčková";
	boxOne.addControl(textOne);

	stackPanel.addControl(boxOne);
	advancedTexture.addControl(stackPanel);

	//NAHRAVANI MODELU
	BABYLON.SceneLoader.OnPluginActivatedObservable.addOnce(loader => {
		loader.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
	});

	BABYLON.SceneLoader.Append("./", "model.glb", scene);
	// Wait for textures and shaders to be ready
	scene.executeWhenReady(function () {
		for (var y = 0; y < scene.meshes.length; y++) {
			scene.meshes[y].checkCollisions = true;
		}
		console.log(scene);
		var gltfScene = scene.meshes[0];
		var gltfAnimations = scene.animationGroups[0];

		for (var i = 0; i < scene.animationGroups.length; i++) {
			scene.animationGroups[i].start(true);
		}



		// Once the scene is loaded, just register a render loop to render it
		engine.runRenderLoop(function () {
			//console.log(scene);
			//console.log(gltfScene);
			scene.render();
		});
	});

	//scene.rotation.y = 2 * Math.PI / 4;





	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw "engine should not be null.";
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
