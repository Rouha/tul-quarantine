let canvas = document.getElementById("renderCanvas");
canvas.requestPointerLock();

let engine = null;
let scene = null;
let sceneToRender = null;

var studentFiles = [
	"bilkova_simple.glb",
	"chenova_simple.glb",
	//["drahonovsky_simple.glb",
	"granova_simple.glb",
	"marinova_simple.glb",
	"kasova_simple.glb",
	"kubik_simple.glb",
	"maskova_simple.glb",
	"metelkova_simple.glb",
	"podestat_simple.glb",
	"reslova_simple.glb",
	"safrova_simple.glb",
	"sedlackova_simple.glb",
	"smrckova_simple.glb",
	"vlach_simple.glb",
	"vu_simple.glb"
];

var studentNames = [
	"Aneta Bílková",
	"Dajana Chenová",
	//"Roman Drahoňovský",
	"Marie Gránová",
	"Eliza Marinova",
	"Markéta Kášová",
	"Štěpán Kubík",
	"Jana Mašková",
	"Marie Metelková",
	"Václav Podestát",
	"Květa Reslová",
	"Sára Šafrová",
	"Karolína Sedláčková",
	"Kateřina Smrčková",
	"Patrik Vlach",
	"Hong Van Vu"
];

var studentFolder = [
	"bilkova",
	"chenova",
	//"drahonovsky",
	"granova",
	"marinova",
	"kasova",
	"kubik",
	"maskova",
	"metelkova",
	"podestat",
	"reslova",
	"sefrova",
	"sedlackova",
	"smrckova",
	"vlach",
	"vu"
];

var studentProjects = [
	"projekt xy",
	"projekt xy",
	//"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy",
	"projekt xy"
];

var studentTexts = [
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	//"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
	"\n Lorem ipsum dolor sit amet, adipiscing elit. Morbi iaculis dolor vel ex porta, id faucibus sapien placerat.",
];

let createDefaultEngine = function() { return new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true }); };

let createScene = function () {
	let scene = new BABYLON.Scene(engine);
	scene.clearColor = new BABYLON.Color4(1,1,1,0.5);

	// Fog
	// scene.fogMode = BABYLON.Scene.FOGMODE_EXP;
	// scene.fogColor = new BABYLON.Color4(0,0,0,0.5);
	// scene.fogDensity = 0.01;
	// scene.fogStart = 20.0;
	// scene.fogEnd = 60.0;

	// Lights
	//new BABYLON.DirectionalLight("light0", new BABYLON.Vector3(-2, -5, 2), scene);
	//new BABYLON.PointLight("light1", new BABYLON.Vector3(2, -5, -2), scene);
	//new BABYLON.PointLight("Omni", new BABYLON.Vector3(10, 50, 50), scene);

	let light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 0, 0), scene);
	light.intensity = 1;

	// Need a free camera for collisions
	let camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(700,-200,100), scene);
	camera.setTarget(new BABYLON.Vector3(0, 1, 1));
	camera.speed = 5;
	camera.fov = 0.8;
	//camera.viewport = 10;
	camera.inverseRotationSpeed = 1.5;
	camera.ellipsoid = new BABYLON.Vector3(2, 2, 2);
	camera.ellipsoidOffset = new BABYLON.Vector3(0,0,0);
	camera.checkCollisions = true;
	camera.attachControl(canvas, true);

	// KEYBOARD
	let overlayElement = document.getElementById("overlayBox");
	overlayElement.onclick = function () {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
		}
		else if (!engine.isPointerLock && canvas.requestPointerLock) {
			canvas.requestPointerLock();
		}
	};

	window.setInterval(function(){
		if (engine.isFullscreen === true) {
			console.log(camera.position);
			console.log("YESSSSSFull");
			stackPanel.isVisible = true;
			cursor.isVisible = true;
		} else {
			console.log("noFull");

			if (typeof stackPanel !== 'undefined') {
				stackPanel.isVisible = false;
			}
			if (typeof cursor !== 'undefined') {
				cursor.isVisible = false;
			}
		}
	}, 50);

	var keyState;
	window.addEventListener('keydown', function (e) {
		keyState = e.key;
		keyboardEvents(keyState);
	}, true);
	window.addEventListener('keyup', function () {
		keyState = 0;
	}, true);

	function keyboardEvents(keyState) {
		if (keyState === "f")
			camera.position.y -= 0.06;
		if (keyState === "r")
			camera.position.y += 0.06;
		if (keyState === "n")
			window.location.replace("../");
		if (keyState === "h") {
			camera.position.x = 700;
			camera.position.y = -200;
			camera.position.z = 100;
			camera.setTarget(new BABYLON.Vector3(0, 1, 1));
		}
		if (keyState === "z") {
			window.location.replace("./");
		}
	}

	// This attaches the camera to the canvas
	camera.keysUp.push(87);
	camera.keysDown.push(83);
	camera.keysLeft.push(65);
	camera.keysRight.push(68);

	let checkNumber = null;
	scene.onPointerDown = function (evt) {
		if (!engine.isFullscreen) {
			engine.switchFullscreen(true); //true = requestPointerLock.
			checkNumber = 0;
		}
		else if(!engine.isPointerLock && canvas.requestPointerLock){
			canvas.requestPointerLock();
			checkNumber = null;
		}
	};

	// GUI
	let advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
	let stackPanel = new BABYLON.GUI.StackPanel();
	stackPanel.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;

	// DISTANCE
	let distanceBox = new BABYLON.GUI.Container('containerOne');
	distanceBox.width = "100%";
	distanceBox.height = "40px";
	distanceBox.paddingBottomInPixels = 1;
	stackPanel.addControl(distanceBox);

	let distanceBoxText = new BABYLON.GUI.Container('containerOneText');
	distanceBoxText.background = "black";
	distanceBoxText.alpha = 1;
	distanceBoxText.topInPixels = 0;
	distanceBoxText.verticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	distanceBox.addControl(distanceBoxText);

	let distanceText = new BABYLON.GUI.TextBlock();
	distanceText.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_CENTER;
	distanceText.textVerticalAlignment = BABYLON.GUI.Control.VERTICAL_ALIGNMENT_TOP;
	distanceText.color = "white";
	distanceText.topInPixels = 0;
	distanceText.fontSize = 16;
	distanceText.text = "ovládání w, a, s, d, r, f + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu | ";
	distanceBox.addControl(distanceText);

	stackPanel.addControl(distanceBox);
	advancedTexture.addControl(stackPanel);

	let cube = null;
	for(let y = 0; y < studentFiles.length; y++) {
		let project = studentFiles[y];
		let randomAxisX = Math.floor( Math.random() * 200) + 50;
		let randomAxisY = Math.floor( Math.random() * 200) + 50;
		let randomAxisZ = Math.floor( Math.random() * 200) + 50;
		let setX = randomTwo() + randomAxisX;
		let setY = randomTwo() + randomAxisY;
		let setZ = randomTwo() + randomAxisZ;

		// Append glTF model to scene.
		BABYLON.SceneLoader.ImportMesh("", "simples/", project, scene, function (newMeshes) {
			let block = newMeshes[0];
			block.position = new BABYLON.Vector3(setX, setY, setZ);
			block.scaling = new BABYLON.Vector3(10,10,10);
			//console.log(block.position);
			cube = new BABYLON.MeshBuilder.CreateBox(y, {height: 40, width: 40, depth: 40}, scene);
			cube.material = new BABYLON.StandardMaterial("mat", scene);
			cube.material.alpha = 0;
			cube.material.diffuseColor = new BABYLON.Color4(0, 0, 0, 0);
			cube.position = new BABYLON.Vector3(setX, setY, setZ);
			cube.checkCollisions = true;

			//block.scaling = new BABYLON.Vector3(7,7,7);

			//console.log(block.scaling.x);
			//console.log(block);

		});

		//console.log("cube[" + y + "][" + j + "] = " + project[j]);
	}

	function randomTwo() {
		let randomNumber = Math.floor(Math.random() * 2);
		if (randomNumber === 0) {
			return "-";
		} else {
			return "";
		}
	}

	// CURSOR
	let cursorTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

	let cursor = new BABYLON.GUI.Rectangle();
	cursor.height = "15px";
	cursor.width = "15px";
	cursor.color = "black";
	cursor.thickness = 2;
	cursorTexture.addControl(cursor);

	function vecToLocal(vector, mesh) {
		let m = mesh.getWorldMatrix();
		return BABYLON.Vector3.TransformCoordinates(vector, m);
	}

	function castRay() {
		let origin = camera.position;
		let forward = new BABYLON.Vector3(0,0,1);
		forward = vecToLocal(forward, camera);

		let direction = forward.subtract(origin);
		direction = BABYLON.Vector3.Normalize(direction);

		let ray = new BABYLON.Ray(origin, direction);
		let hit = scene.pickWithRay(ray);

		distanceText.text = "ovládání w, a, s, d, r, f + pohyb myši, ESC pro ukončení, h - reset pozice, z - obnovení projektu, n - zpět na rozcestník | ";

		if (hit.pickedMesh) {
			let dist = BABYLON.Vector3.Distance(camera.position, hit.pickedMesh.position);
			distanceText.text += " " + Math.round(dist).toString() + " ";
			//console.log(hit.pickedMesh.name);
			let currentId = hit.pickedMesh.name;
			let checkNumber = null;
			if (currentId == 0) {
				distanceText.text += studentNames[0];
				checkNumber = 0;
			}
			if (currentId == 1) {
				distanceText.text += studentNames[1];
				checkNumber = 0;
			}
			if (currentId == 2) {
				distanceText.text += studentNames[2];
				checkNumber = 0;
			}
			if (currentId == 3) {
				distanceText.text += studentNames[3];
				checkNumber = 0;
			}
			if (currentId == 4) {
				distanceText.text += studentNames[4];
				checkNumber = 0;
			}
			if (currentId == 5) {
				distanceText.text += studentNames[5];
				checkNumber = 0;
			}
			if (currentId == 6) {
				distanceText.text += studentNames[6];
				checkNumber = 0;
			}
			if (currentId == 7) {
				distanceText.text += studentNames[7];
				checkNumber = 0;
			}
			if (currentId == 8) {
				distanceText.text += studentNames[8];
				checkNumber = 0;
			}
			if (currentId == 9) {
				distanceText.text += studentNames[9];
				checkNumber = 0;
			}
			if (currentId == 10) {
				distanceText.text += studentNames[10];
				checkNumber = 0;
			}
			if (currentId == 11) {
				distanceText.text += studentNames[11];
				checkNumber = 0;
			}
			if (currentId == 12) {
				distanceText.text += studentNames[12];
				checkNumber = 0;
			}
			if (currentId == 13) {
				distanceText.text += studentNames[13];
				checkNumber = 0;
			}
			if (currentId == 14)	 {
				distanceText.text += studentNames[14];
				checkNumber = 0;
			}
			if (currentId == 15) {
				distanceText.text += studentNames[15];
				checkNumber = 0;
			}

			if (dist < 80) {
				scene.pointerDownPredicate = function() {
					if (checkNumber !== null && Math.round(dist).toString() < 75 && checkNumber !== null){
						engine.switchFullscreen(false); //true = requestPointerLock.
						window.location.replace(studentFolder [hit.pickedMesh.name] + "/");
					}
				}
			}
		}
	}

	scene.registerBeforeRender(function () {
		castRay();
	});

	return scene;
};

engine = createDefaultEngine();
engine.enterPointerlock(true);
if (!engine) throw 'engine should not be null.';
scene = createScene();
sceneToRender = scene;

engine.runRenderLoop(function () {
	if (sceneToRender) {
		sceneToRender.render();
	}
});

window.addEventListener("resize", function () {
	engine.resize();
});
